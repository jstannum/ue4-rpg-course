// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterInterface.h"
#include "MainCharacter.generated.h"

UENUM(BlueprintType)
enum class EMovementStatus : uint8
{
	EMS_Normal UMETA(DisplayName = "Normal"),
	EMS_Sprinting UMETA(DisplayName = "Sprinting"),

	EMS_Max UMETA(DispplayName = "DefaultMax")
};

UCLASS()
class ANDROIDPROJECT01_API AMainCharacter : public ACharacter, public ICharacterInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	/**
	* player stats
	*/
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "PlayerStats")
	float MaxHealth;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PlayerStats")
	float Health;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "PlayerStats")
	float MaxStamina;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PlayerStats")
	float Stamina;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "PlayerStats")
	int32 Coins;

	/**
	* player movement
	*/
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "PlayerMovement")
	EMovementStatus MovementStatus;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "PlayerMovement || PlayerStats")
	float StaminaDrainRate;

	/**
	* weapons
	*/
	/** Currently equipped weapon for character*/
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Items")
	class AWeapon* EquippedWeapon;
	
	/** Item that the player is overlapping with */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Items")
	class AItem* OverlappingItem;

private:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Camera, meta = (AllowPrivateAccess = true))
		class UCameraComponent* FollowCamera;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Camera, meta = (AllowPrivateAccess = true))
		class USpringArmComponent* CameraBoom;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = true))
		float BaseTurnRate;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = true))
		float BaseLookupRate;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = true))
		float SprintModifier;

	float DefaultMaxWalkingSpeed;

	bool bIsSprintPressed;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	void DecrementHealth(float Amount);

	/** Set movement status of the player */
	void SetMovementStatus(EMovementStatus Status);

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	FORCEINLINE void SetEquippedWeapon(class AWeapon* Weapon) { EquippedWeapon = Weapon; }

	virtual void SetOverlappingItem(class AItem* Item) override;

private:
	/**
	* player movement
	*/
	void MoveForward(float Value);

	void MoveRight(float Value);

	void TurnAtRate(float Rate);

	void LookUpAtRate(float Rate);

	void OnSprintStart();

	void OnSprintStop();

	void Die();

	void Interact();

	void Equip(class AWeapon* Weapon);
};
