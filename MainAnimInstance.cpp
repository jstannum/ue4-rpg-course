// Fill out your copyright notice in the Description page of Project Settings.


#include "MainAnimInstance.h"
#include "MainCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

void UMainAnimInstance::NativeInitializeAnimation()
{
	if (Pawn != nullptr) {
		Pawn = TryGetPawnOwner();
		if (Pawn) {
			/**
			Type casting since AMainCharacter inherits from the pawn
			We can force to convert our pawn to MainCharacter
			*/
			MainCharacter = Cast<AMainCharacter>(Pawn);
		}
	}
}

void UMainAnimInstance::UpdateAnimationProperties()
{
	if (Pawn == nullptr)
	{
		Pawn = TryGetPawnOwner();
	}

	if (Pawn)
	{
		FVector Speed = Pawn->GetVelocity();
		// normalize the velocity to remove the Z access which is for the height
		// we exclude height because we're moving not jumping
		FVector LateralSpeed = FVector(Speed.X, Speed.Y, 0.f);
		// Size() will check the length of the vector
		MovementSpeed = LateralSpeed.Size();
		//UE_LOG(LogTemp, Warning, TEXT("%f"), MovementSpeed);

		// movement component has a built in function to check if Pawn is falling
		bIsInAir = Pawn->GetMovementComponent()->IsFalling();

		if (MainCharacter == nullptr)
		{
			MainCharacter = Cast<AMainCharacter>(Pawn);
		}
	}
}
