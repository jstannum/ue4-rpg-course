// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "MainAnimInstance.generated.h"

/**
 *
 */
UCLASS()
class ANDROIDPROJECT01_API UMainAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		float MovementSpeed;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		bool bIsInAir;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		class APawn* Pawn;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
		class AMainCharacter* MainCharacter;

	/** Like a constructor but for animations */
	virtual void NativeInitializeAnimation() override;

	/** Will be called per tick to update the values to be used in animation */
	UFUNCTION(BlueprintCallable, Category = AnimationProperties)
		void UpdateAnimationProperties();

};
