# Animation Blueprint Tips

### Blending
- Use layer blend per bone to create animation base on 2 animations. Helpful for playing separate animation for foot and hand or upper body.
- Use Blend by bool to have conditional animation from 2 state machines.
- Use Slots in montage plus player blend per bone to play montage for upper body only.
- Use anim link to play animation from different ABP but it must have Input Pose.
