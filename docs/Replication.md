# Replication

- For client owned actor 3 functions are needed to execute. One from client, one on server and one with multicast to replicate down to all client. Example, Client sends attack() if it doesn’t have ROLE_Authority then it needs to call the server function, then server will use multicast to send the message to all client. However if the owning actor is also the server it can call the multicast directly.
> Look at [UE4 RPCs docs](https://docs.unrealengine.com/4.27/en-US/InteractiveExperiences/Networking/Actors/RPCs/) table to see how this is executed base on owned actor 
