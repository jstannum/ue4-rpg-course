
# C++ Programming Tips

### `AddDynamic`

- `UFUNCTION()` macro is needed in order for the function to be passed in `AddDynamic`.

```
// on Item.cpp
OnComponentBeginOverlap.AddDynamic(this, &AItem::OnOverlapBegin);

// on Item.h
UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

```
