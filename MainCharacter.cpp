// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/SkeletalMeshSocket.h"
//#include "GameFramework/PlayerController.h"
#include "Weapon.h"
//#include "Item.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create camera boom that pulls towards the player if there's collision
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->TargetArmLength = 600.0f; // set follow distance
	CameraBoom->bUsePawnControlRotation = true; // rotate arm base on controller
	CameraBoom->SetupAttachment(GetRootComponent());

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	// attach camera to end of CameraBoom (Socket)
	FollowCamera->SetupAttachment(CameraBoom, CameraBoom->SocketName);
	FollowCamera->bUsePawnControlRotation = false; // boom will handle the controller rotation

	BaseTurnRate = 65.0f;
	BaseLookupRate = 65.0f;

	// prevent camera boom from rotating the actor
	// controller will only handle the rotation of boom
	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;

	SprintModifier = 1.5f;
	bIsSprintPressed = false;

	// Player Stats
	MaxHealth = 100.0f;
	Health = 100.0f;
	MaxStamina = 100.0f;
	Stamina = 100.0f;
	Coins = 0;
	StaminaDrainRate = 10.0f;
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	GetCharacterMovement()->bOrientRotationToMovement = true; // face the direction of movement
	GetCharacterMovement()->RotationRate = FRotator(0.0f, BaseTurnRate * 9.0f, 0.0f); // at this rotation rate

	//GetCharacterMovement()->JumpZVelocity = 650.f; // height of jump
	GetCharacterMovement()->AirControl = 0.2f; // allow character to move while air
	DefaultMaxWalkingSpeed = GetCharacterMovement()->MaxWalkSpeed;
	MovementStatus = EMovementStatus::EMS_Normal;

}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector Speed = GetVelocity();
	FVector LateralSpeed = FVector(Speed.X, Speed.Y, 0.f);
	float MovementSpeed = LateralSpeed.Size();

	switch (MovementStatus)
	{
	case EMovementStatus::EMS_Normal:
		if (bIsSprintPressed && MovementSpeed > 0 && Stamina > MaxStamina / 4) {
			SetMovementStatus(EMovementStatus::EMS_Sprinting);
		}
		else if (Stamina < MaxStamina)
		{
			Stamina += (StaminaDrainRate / 2) * DeltaTime;
		}
		break;
	case EMovementStatus::EMS_Sprinting:
		if (bIsSprintPressed && MovementSpeed > 0)
		{
			float DeltaDrain = StaminaDrainRate * DeltaTime;
			if (Stamina - DeltaDrain <= 0)
			{
				Stamina = 0;
				SetMovementStatus(EMovementStatus::EMS_Normal);
			}
			else
			{
				Stamina -= DeltaDrain;
			}
		}
		else
		{
			SetMovementStatus(EMovementStatus::EMS_Normal);
		}
		break;
	}

}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	// use parent class ACharacter methods
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// use this to adjust the move speed
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMainCharacter::OnSprintStart);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMainCharacter::OnSprintStop);

	// for any interaction
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AMainCharacter::Interact);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);
	// use parent class APawn methods
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMainCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMainCharacter::LookUpAtRate);

}

void AMainCharacter::DecrementHealth(float Amount)
{
	Health -= Amount;
	if (Health <= 0)
	{

		Die();
	}
}

void AMainCharacter::SetMovementStatus(EMovementStatus Status)
{
	MovementStatus = Status;
	if (MovementStatus == EMovementStatus::EMS_Normal) {
		GetCharacterMovement()->MaxWalkSpeed = DefaultMaxWalkingSpeed;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = DefaultMaxWalkingSpeed * SprintModifier;
	}
}

void AMainCharacter::MoveForward(float Value)
{
	// check if controller is not pointing to null and has a value
	if (Controller != nullptr && Value != 0.0f) {
		// find the forward direction
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.0f, Rotation.Yaw, 0.0f);

		// get the direction as FVector
		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		// add movement input on Direction at Value
		AddMovementInput(Direction, Value);
	}
}

void AMainCharacter::Interact()
{
	if (OverlappingItem && OverlappingItem->GetClass()->IsChildOf<AWeapon>())
	{
		Equip(Cast<AWeapon>(OverlappingItem));
	}
}

void AMainCharacter::Equip(AWeapon* Weapon)
{
	if (EquippedWeapon)
	{
		EquippedWeapon->UnEquip(this);
		SetEquippedWeapon(nullptr);
	}
	Weapon->Equip();
	SetEquippedWeapon(Weapon);
	SetOverlappingItem(nullptr);


	const USkeletalMeshSocket* RightHandSocket = GetMesh()->GetSocketByName("Hand_RSocket");
	if (RightHandSocket)
	{
		// attach this object to character skeletal mesh
		RightHandSocket->AttachActor(Weapon, GetMesh());
	}
}

void AMainCharacter::MoveRight(float Value)
{
	// check if controller is not pointing to null and has a value
	if (Controller != nullptr && Value != 0.0f) {
		// find the forward direction
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.0f, Rotation.Yaw, 0.0f);

		// get the direction as FVector
		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement input on Direction at Value
		AddMovementInput(Direction, Value);
	}
}

void AMainCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookupRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::OnSprintStart()
{
	bIsSprintPressed = true;
}

void AMainCharacter::OnSprintStop()
{
	bIsSprintPressed = false;
}

void AMainCharacter::Die()
{
}

void AMainCharacter::SetOverlappingItem(AItem* Item)
{
	OverlappingItem = Item;
}
