// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloatingPlatform.generated.h"

UCLASS()
class ANDROIDPROJECT01_API AFloatingPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloatingPlatform();
	
	UPROPERTY(VisibleAnywhere, Category = "Platform")
	class UStaticMeshComponent* Platform;

	UPROPERTY(EditAnywhere, Category = "Platform")
	FVector StartPoint;

	UPROPERTY(EditAnywhere, Category = "Platform", meta = (MakeEditWidget))
	FVector EndPoint;

	UPROPERTY(EditAnywhere, Category = "Platform")
	float InterpSpeed;

private:
	FTimerHandle InterpHandle;

	bool bIsInterpolating;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	void ToggleInterpolation();
};
