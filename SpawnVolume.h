// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnVolume.generated.h"

UCLASS()
class ANDROIDPROJECT01_API ASpawnVolume : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawnVolume();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Spawner")
		class UBoxComponent* SpawningBox;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Spawner")
	TSubclassOf<APawn> PawnToSpawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Spawner")
		FVector GetSpawnPoint();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Spawner")
	void SpawnActor(UClass* ToSpawn, const FVector& Location);
};
