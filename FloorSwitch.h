// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FloorSwitch.generated.h"

UCLASS()
class ANDROIDPROJECT01_API AFloorSwitch : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFloorSwitch();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		class UBoxComponent* TriggerBox;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		class UStaticMeshComponent* FloorSwitch;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		class UStaticMeshComponent* Door;

	UPROPERTY(BlueprintReadWrite, Category = "FloorSwitch")
		FVector InitialDoorLocation;

	UPROPERTY(BlueprintReadWrite, Category = "FloorSwitch")
		FRotator InitialDoorRotation;

private:
	FTimerHandle SwitchHandle;

	bool bIsCharacterOnSwitch;

	float SwitchTime;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void UpdateDoorRotation(float YawModifier);

	/** Use this to open door, will be implemented on Blueprint */
	UFUNCTION(BlueprintImplementableEvent, Category = "FloorSwitch")
		void OpenDoor();

	/** Use this to open door, will be implemented on Blueprint */
	UFUNCTION(BlueprintImplementableEvent, Category = "FloorSwitch")
		void CloseDoor();


private:
	UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void OnTimerEnd();

};