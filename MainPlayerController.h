// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ANDROIDPROJECT01_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	/** Reference to the UMG asset in the editor */
	UPROPERTY(EditAnywhere, Category = "Widgets")
	TSubclassOf<class UUserWidget> HUDOverlayAsset;

	/** The actual widget created from the asset */
	UPROPERTY(EditAnywhere, Category = "Widgets")
	class UUserWidget* HUDOverlay;

protected:
	virtual void BeginPlay() override;

};
