// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AndroidProject01GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ANDROIDPROJECT01_API AAndroidProject01GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
