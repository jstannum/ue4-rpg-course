// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

UCLASS()
class ANDROIDPROJECT01_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Item | Collision")
	class USphereComponent* Collider;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Item | Mesh")
	class UStaticMeshComponent* Mesh;

	/** component will show up in the viewport, it is always active */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Item | Particles")
	class UParticleSystemComponent* IdleParticle;

	/** without component it will just be assigned variable and will not showup in viewport unless spawned in world */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Item | Particles")
	class UParticleSystem* OverlapParticle;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Item | Sound")
	class USoundCue* OverlapSound;

	UPROPERTY(EditAnywhere, Category = "Item | Rotation")
	bool bIsRotating;
	
	/** Specify if the particle should rotate with the mesh */
	UPROPERTY(EditAnywhere, Category = "Item | Rotation")
	bool bShouldRotateParticles;

	/** Speed at which the object rotates with time */
	UPROPERTY(EditAnywhere, Category = "Item | Rotation")
	float RotationSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
