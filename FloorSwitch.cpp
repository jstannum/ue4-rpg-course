// Fill out your copyright notice in the Description page of Project Settings.


#include "FloorSwitch.h"
#include "Components/BoxComponent.h"

// Sets default values
AFloorSwitch::AFloorSwitch()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	SetRootComponent(TriggerBox);

	// this steps is like manually overriding the collision profile presets
	TriggerBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly); // will not block character but will trigger collision
	TriggerBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic); // set object type to world static
	TriggerBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore); // ignore all
	TriggerBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap); // apply overlap on pawns only

	FloorSwitch = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorSwitch"));
	FloorSwitch->SetupAttachment(GetRootComponent());

	Door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door"));

	bIsCharacterOnSwitch = false;
	SwitchTime = 2.0f;
}

// Called when the game starts or when spawned
void AFloorSwitch::BeginPlay()
{
	Super::BeginPlay();

	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &AFloorSwitch::OnBeginOverlap);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &AFloorSwitch::OnEndOverlap);

	InitialDoorLocation = Door->GetComponentLocation();
	InitialDoorRotation = Door->GetComponentRotation();

}

// Called every frame
void AFloorSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFloorSwitch::UpdateDoorRotation(float YawModifier)
{
	FRotator NewRotation = InitialDoorRotation;
	NewRotation.Yaw += YawModifier * 90.0f;
	Door->SetWorldRotation(NewRotation);
}

void AFloorSwitch::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlap begin"));
	//if we're going to use clear timer there's no need to check character on switch and call another function
	//i didn't use it since it's good to have 2 ways in the code as this is just for demo
	//GetWorldTimerManager().ClearTimer(SwitchHandle);
	if (!bIsCharacterOnSwitch) {
		bIsCharacterOnSwitch = true;
		OpenDoor();
	}
}

void AFloorSwitch::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlap end"));
	if (bIsCharacterOnSwitch) {
		bIsCharacterOnSwitch = false;
		GetWorldTimerManager().SetTimer(SwitchHandle, this, &AFloorSwitch::OnTimerEnd, SwitchTime);
	}
}

void AFloorSwitch::OnTimerEnd()
{
	if (!bIsCharacterOnSwitch) {
		CloseDoor();
	}
}
