// Fill out your copyright notice in the Description page of Project Settings.


#include "BoatPawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "BoatPawnMovementComponent.h"

// Sets default values
ABoatPawn::ABoatPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetWorldRotation(FRotator(0.0f, 0.0f, 180.0f));
	//Mesh->SetupAttachment(GetRootComponent());
	SetRootComponent(Mesh);

	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	CameraSpringArm->SetupAttachment(GetRootComponent());
	CameraSpringArm->TargetArmLength = 700.0f;
	CameraSpringArm->SetRelativeRotation(FRotator(-60.0f, 0.0f, 0.0f));
	CameraSpringArm->bEnableCameraLag = true;
	CameraSpringArm->CameraLagSpeed = 3.0f;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(CameraSpringArm, CameraSpringArm->SocketName);
	//CameraComponent->SetupAttachment(CameraSpringArm, USpringArmComponent::SocketName);

	CurrentVelocity = FVector();
	MoveSpeed = 100.0f;

	BoatMovementComponent = CreateDefaultSubobject<UBoatPawnMovementComponent>(TEXT("BoatMovementComponent"));
	BoatMovementComponent->UpdatedComponent = GetRootComponent();

	CameraInput = FVector2D();
}

// Called when the game starts or when spawned
void ABoatPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ABoatPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// only used when add movement input is not used
	/*FVector CurrentLocation = GetActorLocation();
	CurrentLocation += MoveSpeed * CurrentVelocity * DeltaTime;
	SetActorLocation(CurrentLocation);*/

	// this will rotate the actor, including all component
	FRotator NewRotation = GetActorRotation();
	//NewRotation.Pitch += CameraInput.Y;
	NewRotation.Yaw += CameraInput.X;
	SetActorRotation(NewRotation);

	// this will rotate spring arm only for camera
	FRotator SpringArmRotation = CameraSpringArm->GetComponentRotation();
	SpringArmRotation.Pitch = FMath::Clamp(SpringArmRotation.Pitch += CameraInput.Y, -89.0f, 89.0f);
	//SpringArmRotation.Yaw += CameraInput.X;

	CameraSpringArm->SetWorldRotation(SpringArmRotation);

}

// Called to bind functionality to input
void ABoatPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ABoatPawn::MoveX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ABoatPawn::MoveY);
	PlayerInputComponent->BindAxis(TEXT("CameraPitch"), this, &ABoatPawn::PitchCamera);
	PlayerInputComponent->BindAxis(TEXT("CameraYaw"), this, &ABoatPawn::YawCamera);
}

void ABoatPawn::MoveX(float Input)
{
	//CurrentVelocity.X = FMath::Clamp(Input, -1.0f, 1.0f);
	FVector Forward = GetActorForwardVector();
	AddMovementInput(Forward * Input);
}

void ABoatPawn::MoveY(float Input)
{
	//CurrentVelocity.Y = FMath::Clamp(Input, -1.0f, 1.0f);
	FVector Right = GetActorRightVector();
	AddMovementInput(Right * Input);
}

void ABoatPawn::PitchCamera(float AxisValue)
{
	CameraInput.Y = AxisValue;
}

void ABoatPawn::YawCamera(float AxisValue)
{
	CameraInput.X = AxisValue;
}

UPawnMovementComponent* ABoatPawn::GetMovementComponent() const
{
	return BoatMovementComponent;
}
