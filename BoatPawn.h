// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BoatPawn.generated.h"

UCLASS()
class ANDROIDPROJECT01_API ABoatPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABoatPawn();

	UPROPERTY(VisibleAnywhere, Category = "BoatCamera")
		class UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, Category = "BoatCamera")
		class USpringArmComponent* CameraSpringArm;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float MoveSpeed;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		class UBoatPawnMovementComponent* BoatMovementComponent;

private:
	FVector CurrentVelocity;
	FVector2D CameraInput;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual UPawnMovementComponent* GetMovementComponent() const override;

private:
	void MoveX(float Input);
	void MoveY(float Input);

	void PitchCamera(float AxisValue);
	void YawCamera(float AxisValue);
};