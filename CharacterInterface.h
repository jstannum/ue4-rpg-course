// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CharacterInterface.generated.h"

/**
 * Implement function equip and set overlap for actor
 */
UINTERFACE(MinimalAPI)
class UCharacterInterface : public UInterface
{
	GENERATED_BODY()
	
};

class ANDROIDPROJECT01_API ICharacterInterface
{
	GENERATED_BODY()

public:
	/** Set last overlapped item for actor */
	UFUNCTION()
	virtual void SetOverlappingItem(class AItem* Item) = 0;
};
