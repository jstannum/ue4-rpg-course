// Fill out your copyright notice in the Description page of Project Settings.


#include "FloatingPlatform.h"

// Sets default values
AFloatingPlatform::AFloatingPlatform()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Platform = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform"));
	SetRootComponent(Platform);

	EndPoint = FVector(5.0f, 0.0f, 0.0f);
	InterpSpeed = 50.0f;
	bIsInterpolating = true;
}

// Called when the game starts or when spawned
void AFloatingPlatform::BeginPlay()
{
	Super::BeginPlay();

	StartPoint = GetActorLocation();

	// by default EndPoint will be relative/local position
	// we need to add the actor location to get the world position
	EndPoint += StartPoint;
}

// Called every frame
void AFloatingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	

	if (bIsInterpolating) {
		FVector CurrentLoc = GetActorLocation();
		FVector Interp = FMath::VInterpConstantTo(CurrentLoc, EndPoint, DeltaTime, InterpSpeed);
		SetActorLocation(Interp);

		// size will get the magnitude of vector
		// by subtracting we will get the distance between two points
		if ((CurrentLoc - EndPoint).Size() < 0.1f) {
			// if end point is already reached we swap them
			// so that the platform will be go back and fort
			// https://docs.unrealengine.com/en-US/API/Runtime/Core/Templates/Swap/1/index.html
			Swap(StartPoint, EndPoint);

			// toggle interpolation off then set timer to enable it after 2 seconds
			ToggleInterpolation();
			GetWorldTimerManager().SetTimer(InterpHandle, this, &AFloatingPlatform::ToggleInterpolation, 2.0f);
		}
	}


}

void AFloatingPlatform::ToggleInterpolation()
{
	bIsInterpolating = !bIsInterpolating;
}

